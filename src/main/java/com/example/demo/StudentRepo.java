package com.example.demo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepo extends JpaRepository<Student, Integer>{

	
    @Query(value = "SELECT unique_id FROM MY_DATA_TABLE where NAME=:name", nativeQuery = true)
    public int findByName(@Param ("name") String name);
}
