package com.example.demo;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class StudentService {

	@Autowired
	StudentRepo sRepo;
	
	public Student createInstanceInDB(int id, String name, String password)
	{
		Student s = new Student();
		s.setUniqueId(id);
		s.setName(name);
		s.setPassword(password);
		return s;
	}
	
	
	public String saveStudent(Student st)
	{
		sRepo.save(st);
		return "data saved";
	}
	
	public List<Student> getAllStudent()
	{
		List <Student> list = sRepo.findAll();
		return list;
	}
	
	public Student getStudentById( int id) 
	{
		Optional<Student> student = sRepo.findById(id);
		
		if(student.isPresent())
		{
		
			return student.get();
		}
		else
			return null;
	}
	
	public Student createOrUpdateStudent(int id)
	{
		Optional <Student> student = sRepo.findById(id);
		
		if(student.isPresent())
		{
			Student st = student.get();
			st.setPassword("PASSWORD");
			sRepo.save(st);
			return st;
		}
		else
			return null;
	}
	
	public void deleteStudent(int id)
	{
		Optional <Student> student = sRepo.findById(id);
		
		if(student.isPresent())
		{
			Student st = student.get();
			sRepo.delete(st);
		}
	}
	
	
	public boolean validateCreds(int id, String name, String password)
	{
		int getId = sRepo.findByName(name);
		
		Optional <Student> checkDB = sRepo.findById(getId);
	
		
		if(checkDB.isPresent())
		{
			Student st = checkDB.get();
			
			if((st.getName().equals(name)) && (st.getPassword().equals(password)))
				return true;
			else
				
				return false;
		}
		else
			return false;
	}
	
}
	