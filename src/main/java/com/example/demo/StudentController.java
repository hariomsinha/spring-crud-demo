package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;


@RestController
public class StudentController {
	
	@Autowired
	StudentService sService;
	
	
	@RequestMapping("/student")
	public ResponseEntity<List<Student>> getAllStudent()
	{
		List <Student> list = sService.getAllStudent();
		
		return new ResponseEntity<List<Student>>(list,new HttpHeaders(),HttpStatus.OK);
	}
	
	@GetMapping("/{id}")

	public ResponseEntity<Student> getStudentById( @PathVariable ("id") int id)
	{
		Student student= sService.getStudentById(id);
		
		return new ResponseEntity<Student> (student,new HttpHeaders(), HttpStatus.OK);
		
	}
	
	@PostMapping("/{id}")
	public ResponseEntity<Student> createOrUpdateStudent(@PathVariable ("id") int id)
	{
		Student student = sService.createOrUpdateStudent(id);
		
		return new ResponseEntity<Student>(student, new HttpHeaders(), HttpStatus.OK);
		
		
	}
	
	@DeleteMapping("/{id}")
	public HttpStatus deleteStudent(@PathVariable ("id") int id)
	{
		sService.deleteStudent(id);
		return HttpStatus.FORBIDDEN;
	}
	
	
	@RequestMapping(value = "/jspPage", method = RequestMethod.GET)
	public ModelAndView jspPage(ModelMap mp)
	{	
		
		ModelAndView mv = new ModelAndView();
		mv.setViewName("homepage");
		return mv;
	}
	
	@PostMapping("/loginpage")
	public ModelAndView loginpage(@RequestParam int uniqueId, @RequestParam String name, @RequestParam String password)
	{
			
		ModelAndView mv = new ModelAndView();
		Student st = sService.createInstanceInDB(uniqueId, name, password);
		String msg = sService.saveStudent(st);
		mv.addObject("username",name);
		mv.addObject("msg", msg);
		mv.setViewName("loginpage");
		return mv;
	}
	
	@PostMapping("/Successfulloginpage")
	public ModelAndView Successfulloginpage (@RequestParam int uniqueId, @RequestParam String name, @RequestParam String password)
	{
		ModelAndView mv = new ModelAndView();
		boolean flag = sService.validateCreds(uniqueId,name,password);
		if(flag == true)
		{
		mv.setViewName("Successfulloginpage");
		}
		else
		{
		mv.setViewName("404Page");	
		}
		return mv;
		
		
		
	}

}
